# Terraform Deployment for GitLab on GKE

## Description

Deploys a Production ready GitLab environment on Google Kubernetes Engine.

## Deployment Prerequisites

### GCP
* Create a Google Cloud Account
* Create a new project
* Use us-central1 as your region
* Create Service Account with keys and correct permissions

### Enable GCP API's
* Kubernetes Engine API
* Compute Engine API
* IAM API
* Cloud Resource Manager API

### Local
* Install Terraform locally
* Install gcloud and kubectl packages locally
* Remember to set variable values in vars.tf

## Terrafrom State Management

Terraform state is stored within a bucket named "terraform-state". The bucket is created during the deployment.

## Deployment consists of the below scripts

The deployment contains the below scripts.

1. vars.tf
1. teraform_backend_state_config.tf
1. deploy.tf
1. outputs.tf
1. values.yaml.tf

## GCP Services used in the deployment

* Kubernetes Engine
* Storage
* IAM
* Compute Engine
* VPC Network
* SQL
* Redis
* Cloud Resource Manager

## Notes

To deploy the script, navigate to the working directory where the package was extracted and run the following commands in your terminal session.

* terraform init
* terraform plan
* terraform apply

To destroy all resources created run the below command.

* terraform destroy
