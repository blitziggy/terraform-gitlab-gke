#Config values for GitLab chart on GKE
global:
  edition: ce
  hosts:
    https: true
    gitlab: {}
    externalIP: var.INGRESS_IP
    ssh: ~

#Configure Ingress Settings
  ingress:
    configureCertmanager: true
    enabled: true
    tls:
      enabled: true

#Configure Postgresql Settings
  psql:
    password:
      secret: gitlab-pg
      key: password
    host: var.DB_PRIVATE_IP
    port: 5432
    username: gitlab
    database: gitlabhq_production

  redis:
    password:
      enabled: false
    host: var.REDIS_PRIVATE_IP

#Configure Minio Settings
  minio:
    enabled: false

#Configure Appconfig Settings
  appConfig:
#General App Settings
    enableUsagePing: false

#LFS Artifacts Uploads Packages
    backups:
      bucket: var.PROJECT_ID-gitlab-backups
    lfs:
      bucket: var.PROJECT_ID-git-lfs
      connection:
        secret: gitlab-rails-storage
        key: connection
    artifacts:
      bucket: var.PROJECT_ID-gitlab-artifacts
      connection:
        secret: gitlab-rails-storage
        key: connection
    uploads:
      bucket: var.PROJECT_ID-gitlab-uploads
      connection:
        secret: gitlab-rails-storage
        key: connection
    packages:
      bucket: var.PROJECT_ID-gitlab-packages
      connection:
        secret: gitlab-rails-storage
        key: connection

Pseudonymizer Settings
    pseudonymizer:
      bucket: var.PROJECT_ID-gitlab-pseudo
      connection:
        secret: gitlab-rails-storage
        key: connection

certmanager-issuer:
  email: var.CERT_MANAGER_EMAIL

prometheus:
  install: false

redis:
  install: false

gitlab:
  gitaly:
    persistence:
      size: 200Gi
      storageClass: "pd-ssd"
  task-runner:
    backups:
      objectStorage:
        backend: gcs
        config:
          secret: google-application-credentials
          key: gcs-application-credentials-file
          gcpProject: var.PROJECT_ID

postgresql:
  install: false

gitlab-runner:
  install: var.GITLAB_RUNNER_INSTALL
  rbac:
    create: true
  runners:
    locked: false
    cache:
      cacheType: gcs
      gcsBucketName: var.PROJECT_ID-runner-cache
      secretName: google-application-credentials
      cacheShared: true

registry:
  enabled: true
  storage:
    secret: gitlab-registry-storage
    key: storage
    extraKey: gcs.json
