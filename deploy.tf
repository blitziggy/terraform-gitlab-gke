#PROVIDERS
provider "google" {
  project = var.project_id
  credentials = file("gitlab-gcp-302414-0de68bd46bc1.json")
  region  = var.region
}

module "gke_auth" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  version = "~> 9.1"

  project_id   = var.project_id
  cluster_name = module.gke.name
  location     = module.gke.location

  depends_on = [time_sleep.sleep_for_cluster_fix_helm_6361]
}

provider "helm" {
  kubernetes {
    cluster_ca_certificate = module.gke_auth.cluster_ca_certificate
    host                   = module.gke_auth.host
    token                  = module.gke_auth.token
  }
}

provider "kubernetes" {
  load_config_file = false

  cluster_ca_certificate = module.gke_auth.cluster_ca_certificate
  host                   = module.gke_auth.host
  token                  = module.gke_auth.token
}

#******************************************************************************#

#ENABLE GCP SERVICES
resource "google_project_service" "gcp_services" {
  count   = length(var.gcp_service_list)
  project = var.project_id
  service = var.gcp_service_list[count.index]
  disable_dependent_services = true
}

#******************************************************************************#

#CREATE GCP IAM OBJECTS
#SERVICE ACCOUNT AND KEY
resource "google_service_account" "gitlab-gcs" {
  account_id  = var.service_account_id
  display_name = "GitLab Cloud Storage"
  depends_on = [google_project_service.gcp_services]
}

#SERVICE ACCOUNT KEY
resource "google_service_account_key" "gitlab-gcs" {
  service_account_id = google_service_account.gitlab-gcs.name
  depends_on = [google_project_service.gcp_services]
}

#SERVICE ACCOUNT ROLE BINDING
resource "google_project_iam_member" "gitlab-gcs" {
  project = var.project_id
  role    = "roles/storage.admin"
  member  = "serviceAccount:${google_service_account.gitlab-gcs.email}"
  depends_on = [google_project_service.gcp_services]
}

#******************************************************************************#

#CREATE GCP BUCKETS

resource "google_storage_bucket" "terraform-state" {
  name     = "${var.project_id}-terraform-state"
  location = var.region
}

resource "google_storage_bucket" "gitlab-backups" {
  name     = "${var.project_id}-gitlab-backups"
  location = var.region
}

resource "google_storage_bucket" "git-lfs" {
  name     = "${var.project_id}-git-lfs"
  location = var.region
}

resource "google_storage_bucket" "gitlab-artifacts" {
  name     = "${var.project_id}-gitlab-artifacts"
  location = var.region
}

resource "google_storage_bucket" "gitlab-uploads" {
  name     = "${var.project_id}-gitlab-uploads"
  location = var.region
}

resource "google_storage_bucket" "gitlab-packages" {
  name     = "${var.project_id}-gitlab-packages"
  location = var.region
}

resource "google_storage_bucket" "gitlab-pseudo" {
  name     = "${var.project_id}-gitlab-pseudo"
  location = var.region
}

resource "google_storage_bucket" "runner-cache" {
  name     = "${var.project_id}-runner-cache"
  location = var.region
}

resource "google_storage_bucket" "gitlab-registry" {
  name     = "${var.project_id}-registry"
  location = var.region
}

#******************************************************************************#

#CONFIGURE TERRAFORM STATE
terraform {
  backend "gcs" {
    bucket  = "${var.project_id}-terraform-state"
    prefix  = "terraform/state"
  }
}

#******************************************************************************#

#GCP NETWORKING
#CREATE NETWORK
resource "google_compute_network" "gitlab-network" {
  name                    = var.gitlab_network_name
  project                 = var.project_id
  auto_create_subnetworks = false
  depends_on = [google_project_service.gcp_services]
}

#CREATE PRIVATE SUBNETS
resource "google_compute_subnetwork" "gitlab-subnetwork" {
  name          = var.gitlab_subnetwork_name
  ip_cidr_range = var.gitlab_nodes_subnet_cidr
  region        = var.region
  network       = google_compute_network.gitlab-network.id
  depends_on = [google_project_service.gcp_services, google_compute_network.gitlab-network]

  secondary_ip_range {
    range_name    = "gitlab-cluster-pod-cidr"
    ip_cidr_range = var.gitlab_pods_subnet_cidr
  }

  secondary_ip_range {
    range_name    = "gitlab-cluster-service-cidr"
    ip_cidr_range = var.gitlab_services_subnet_cidr
  }
}

#CREATE EXTERNAL IP FOR GITLAB INSTANCE
resource "google_compute_address" "gitlab" {
  name = var.gitlab_network_name
  address_type = "EXTERNAL"
  region = var.region
  description  = "Gitlab Ingress IP"
  count = var.gitlab_network_name == "" ? 1 : 0
  depends_on  = [google_project_service.gcp_services, google_project_iam_member.gitlab-gcs, google_project_service.gcp_services]
}

#CREATE PRIVATE IP ADDRESS RANGE FOR POSTGRESQL
resource "google_compute_global_address" "gitlab_sql" {
  provider = google
  project = var.project_id
  name  = var.psql_db_name
  purpose  = "VPC_PEERING"
  address_type  = "INTERNAL"
  network  = google_compute_network.gitlab-network.id
  address       = "10.1.0.0"
  prefix_length = 16
  depends_on = [google_project_service.gcp_services, google_compute_network.gitlab-network]
}

#CONFIGURE VPC PEERING
resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google
  network  = google_compute_network.gitlab-network.id
  service  = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.gitlab_sql.name]
  depends_on = [google_project_service.gcp_services, google_compute_network.gitlab-network]
}

#******************************************************************************#

#CREATE POSTGRESQL INSTANCE
resource "google_sql_database_instance" "gitlab-psql-db" {
  depends_on = [google_service_networking_connection.private_vpc_connection, google_project_service.gcp_services]
  name = var.psql_db_instance_name
  database_version = "POSTGRES_13"
  region = var.region
  deletion_protection=false

  settings {
    tier            = "db-custom-4-15360"
    disk_autoresize = true

    ip_configuration {
      ipv4_enabled    = "false"
      private_network = google_compute_network.gitlab-network.id
    }
  }
}

#CREATE POSTGRESQL DATABASE
resource "google_sql_database" "gitlabhq_production" {
  name     = var.psql_db_name
  instance = google_sql_database_instance.gitlab-psql-db.name
  depends_on = [google_project_service.gcp_services, google_sql_database_instance.gitlab-psql-db]
}

#******************************************************************************#

#CREATE POSTGRESQL DB USER PASSWORD
resource "random_string" "db_user_password" {
  length  = 16
  special = false
  depends_on = [google_project_service.gcp_services, google_sql_database_instance.gitlab-psql-db]
}

#CREATE POSTGRESQL DB USER
resource "google_sql_user" "db_user_name" {
  name     = "psql-user"
  instance = google_sql_database_instance.gitlab-psql-db.name
  password = random_string.db_user_password.result
  depends_on = [google_project_service.gcp_services, google_sql_database_instance.gitlab-psql-db]
}

#******************************************************************************#

#CREATE REDIS INSTANCE
resource "google_redis_instance" "gitlab-redis-db" {
  name = var.redis_db_instance_name
  display_name = "GitLab Redis"
  tier = "STANDARD_HA"
  memory_size_gb = 5
  region  = var.region
  authorized_network = google_compute_network.gitlab-network.id
  depends_on = [google_project_service.gcp_services, google_compute_network.gitlab-network]
}

#******************************************************************************#

#CREATE GKE CLUSTER
module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google"
  version = "~> 12.0"

  # Create an implicit dependency on service activation
  project_id = var.project_id
  name = var.gke_cluster_name
  region = var.region
  regional = true
  kubernetes_version = var.gke_version
  remove_default_node_pool = true
  initial_node_count       = 1

  network           = google_compute_network.gitlab-network.name
  subnetwork        = google_compute_subnetwork.gitlab-subnetwork.name
  ip_range_pods     = "gitlab-cluster-pod-cidr"
  ip_range_services = "gitlab-cluster-service-cidr"
  issue_client_certificate = true

  node_pools = [
    {
      name         = var.gke_cluster_name
      autoscaling  = false
      machine_type = var.gke_node_type
      node_count   = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = ["https://www.googleapis.com/auth/cloud-platform"]
  }
}

#******************************************************************************#

#CREATE GKE CLUSTER STORAGE CLASS
resource "kubernetes_storage_class" "pd-ssd" {
  metadata {
    name = "pd-ssd"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  parameters = {
    type = "pd-ssd"
  }
  depends_on = [google_project_service.gcp_services, time_sleep.sleep_for_cluster_fix_helm_6361]
}

#******************************************************************************#

#CREATE GKE SECRET FOR POSTGRESQL DATABASE
resource "kubernetes_secret" "gitlab_pg" {
  metadata {
    name = "gitlab-pg"
  }

  data = {
    password = var.psql_pass != "" ? var.psql_pass : random_string.db_user_password.result
  }
  depends_on = [google_project_service.gcp_services, time_sleep.sleep_for_cluster_fix_helm_6361]
}

#******************************************************************************#

#CREATE GKE SECRET FOR GITLAB REGISTRY

resource "kubernetes_secret" "gitlab_registry_storage" {
  metadata {
    name = "gitlab-registry-storage"
  }

  data = {
    "gcs.json" = <<EOT
${base64decode(google_service_account_key.gitlab-gcs.private_key)}
EOT
    storage    = <<EOT
gcs:
  bucket: ${var.project_id}-registry
  keyfile: /etc/docker/registry/storage/gcs.json
EOT
  }

  depends_on = [google_project_service.gcp_services, time_sleep.sleep_for_cluster_fix_helm_6361]
}

#CREATE GKE SECRET FOR CLOUD STORAGE CREDENTIALS
resource "kubernetes_secret" "gitlab_gcs_credentials" {
  metadata {
    name = "google-application-credentials"
  }

  data = {
    gcs-application-credentials-file = base64decode(google_service_account_key.gitlab-gcs.private_key)
  }
  depends_on = [google_project_service.gcp_services, time_sleep.sleep_for_cluster_fix_helm_6361]
}

#******************************************************************************#

#CREATE GKE SECRET FOR RAILS PARAMETERS
resource "kubernetes_secret" "gitlab_rails_storage" {
  metadata {
    name = "gitlab-rails-storage"
  }

  data = {
    connection = <<EOT
provider: Google
google_project: ${var.project_id}
google_client_email: ${google_service_account.gitlab-gcs.email}
google_json_key_string: '${base64decode(google_service_account_key.gitlab-gcs.private_key)}'
EOT
  }
  depends_on = [google_project_service.gcp_services, time_sleep.sleep_for_cluster_fix_helm_6361]

}

#******************************************************************************#

#GITLAB INSTALLATION
#FETCH DATA AND LOCAL VALUES
data "google_compute_address" "gitlab" {
  name   = var.gitlab_network_name
  region = var.region

  count = var.gitlab_network_name == "" ? 0 : 1
}

locals {
  gitlab_address = var.gitlab_network_name == "" ? google_compute_address.gitlab.0.address : data.google_compute_address.gitlab.0.address
}

#******************************************************************************#

#CONFIGURE CHART
#SET VALUES FOR HELM CHART
data "template_file" "helm_values" {
  template = file("values.yaml.tpl")

  vars = {
    INGRESS_IP            = local.gitlab_address
    DB_PRIVATE_IP         = google_sql_database_instance.gitlab-psql-db.private_ip_address
    REDIS_PRIVATE_IP      = google_redis_instance.gitlab-redis-db.host
    PROJECT_ID            = var.project_id
    CERT_MANAGER_EMAIL    = var.service_account_email
    GITLAB_RUNNER_INSTALL = var.gitlab_runner_install
  }
}

#******************************************************************************#

#CLUSTER SLEEP
resource "time_sleep" "sleep_for_cluster_fix_helm_6361" {
  create_duration  = "180s"
  destroy_duration = "180s"
  depends_on       = [module.gke.endpoint, google_sql_database.gitlabhq_production]
}

#******************************************************************************#

#GET STATUS OF HELM CHART ON GKE CLUSTER
resource "helm_release" "gitlab" {
  name       = "gitlab"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab"
  version    = var.helm_chart_version
  timeout    = 1200

  values = [data.template_file.helm_values.rendered]

  depends_on = [
    google_redis_instance.gitlab-redis-db,
    google_sql_user.db_user_name,
    kubernetes_storage_class.pd-ssd,
    kubernetes_secret.gitlab_pg,
    kubernetes_secret.gitlab_rails_storage,
    kubernetes_secret.gitlab_registry_storage,
    kubernetes_secret.gitlab_gcs_credentials,
    time_sleep.sleep_for_cluster_fix_helm_6361,
    google_project_service.gcp_services
  ]
}
