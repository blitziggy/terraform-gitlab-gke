#All variables used in main deployment

variable "project_id" {
  description = "GCP Project to deploy resources"
  default = "gitlab-gcp-302414"
}

variable "region" {
  description = "Region to deploy resources"
  default = "us-central1"
}

variable "gcp_service_list" {
  description = "List of gcp services apis to enable"
  default = ["compute.googleapis.com", "container.googleapis.com", "servicenetworking.googleapis.com", "cloudresourcemanager.googleapis.com", "redis.googleapis.com"]
}

variable "service_account_id" {
  description = "ID for GCP Service Account"
  default = "gitlab-gcs-demo"
}

variable "service_account_email" {
  description = "Email address for GCP Service Account"
  default = "me@example.com"
}

variable "gitlab_network_name" {
  description = "Name of the address to use for GitLab ingress"
  default     = "gitlab-network"
}

variable "gitlab_subnetwork_name" {
  description = "Name of the address to use for GitLab ingress"
  default     = "gitlab-subnetwork"
}

variable "gitlab_nodes_subnet_cidr" {
  default     = "10.0.0.0/16"
  description = "Cidr range to use for gitlab GKE nodes subnet"
}

variable "gitlab_pods_subnet_cidr" {
  default     = "10.3.0.0/16"
  description = "Cidr range to use for gitlab GKE pods subnet"
}

variable "gitlab_services_subnet_cidr" {
  default     = "10.2.0.0/16"
  description = "Cidr range to use for gitlab GKE services subnet"
}

variable "gke_cluster_name" {
  description = "Name for GKE cluster"
  default = "gitlab-gke"
}

variable "gke_cluster_nodepool_name" {
  description = "Name for GKE cluster"
  default = "gitlab-gke-cluster-node-pool"
}

variable "gke_node_type" {
  description = "VM Type for GKE Nodes"
  default = "e2-medium"
}

variable "psql_db_instance_name" {
  description = "Instance database name for PostgreSQL db"
  default = "gitlab-sql-db-5"
}

variable "redis_db_instance_name" {
  description = "Instance database name for PostgreSQL db"
  default = "gitlab-redis-db-1"
}

variable "psql_db_name" {
  description = "Database name for PostgreSQL db"
  default = "gitlabhq-production"
}

variable "psql_user" {
  description = "Username for PostgreSQL db"
  default = ""
}

variable "psql_pass" {
  description = "Password for PostgreSQL db"
  default = ""
}

variable "gitlab_runner_install" {
  description = "Choose whether to install the gitlab runner in the cluster"
  default     = true
}

variable "helm_chart_version" {
  type        = string
  default     = "4.2.4"
  description = "Helm chart version to install during deployment"
}

variable "gke_version" {
  description = "Version of GKE to use for the GitLab cluster"
  default     = "1.16"
}
